<h1 align="center">scrimba MemeGenerator Project</h1>

<div align="center">
   Solution for a challenge from  <a href="https://scrimba.com/dashboard#overview" target="_blank">scrimba.com</a>.
</div>

<div align="center">
  <h3>
    <a href="https://scrimbamemegeneratorrajashekarmanda.netlify.app/">
      Demo
    </a>
    <span> | </span>
    <a href="https://gitlab.com/rajashekarmanda3729/scrimbamemegeneratorreactjsproject">
      Solution
    </a>
    <span> | </span>
    <a href="https://www.figma.com/file/goMBCKN2nl3iDp6qVmby6m/Meme-Generator-(Copy)?node-id=0-1&t=nDISK32WpfhQNsRl-0">
      Challenge
    </a>
  </h3>
</div>

### UI Design 
![](src/assets/demo.png)


### Setup
* if you want to start project with vite@reactJS use below command
*         npm create vite@latest
*         npm install
*         npm run dev
*         npm run build

the above commans for server run/install depedencies and build code.

### ReactJS Functionalities
* Used react-components & ReactDOM 
* Used CSS Flexbox for styling
* Used third party packages
* Having Memes url data in source folder to get random one.
* react State where we render depends on data.
* random() functionalities for get random url of image.
* form element when user press enter button it will update url give new random image.

### Features
  * User can Generate random memes.
  * By clicking button every time it will show new Meme image.
  * User can write top/bottom text/comment for that meme image.

### Contact

* Github [@rajashekarmanda](https://github.com/Rajashekarmanda)