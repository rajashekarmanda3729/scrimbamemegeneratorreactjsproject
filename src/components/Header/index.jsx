import Vector from '../../assets/Vector.png'
import './style.css'

function Header() {
    return (
        <div className='header-container'>
            <div className='header-logo-container'>
                <img src={Vector} alt="vector" className='logo'/>
                <h1 className='logo-heading'>Meme Generator</h1>
            </div>
            <div className='header-content-container'>
                <p className='course-heading'>React Course Project - 3</p>
            </div>
        </div>
    )
}
export default Header