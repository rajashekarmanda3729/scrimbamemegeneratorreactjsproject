import React from "react"
import memeimg from '../../assets/memeimg.png'
import './style.css'

function Meme() {

    const [meme, setMeme] = React.useState(
        {
            topText: '',
            bottomText: '',
            randomUrl: memeimg
        })

    const [memesData, setMemesData] = React.useState([])

    React.useEffect(() => {
        fetch("https://api.imgflip.com/get_memes")
            .then(response => response.json())
            .then(data => setMemesData(data.data.memes))
    }, [])

    function onChangeImage(event) {
        event.preventDefault()
        const randomNumber = Math.floor(Math.random() * memesData.length)
        const randomImageUrl = memesData[randomNumber].url
        setMeme({ ...meme, randomUrl: randomImageUrl })
    }

    function onChangeTopText(event) {
        setMeme({ ...meme, topText: event.target.value })
    }

    function onChangeBottomText(event) {
        setMeme({ ...meme, bottomText: event.target.value })
    }

    return (
        <main className="meme-container">
            <form className="input-container" onSubmit={onChangeImage}>
                <div className="input-boxes-container">
                    <input type="text" className="input" placeholder="Enter Meme Top text"
                        onChange={onChangeTopText} value={meme.topText} />
                    <input type="text" className="input" placeholder="Enter Meme Bottom text"
                        onChange={onChangeBottomText} value={meme.bottomText} />
                </div>
                <button type="submit" className="button">
                    Get a new meme image @
                </button>
            </form>
            <div className="image-container">
                <img src={meme.randomUrl} alt="memeImage" className="meme-image" />
                <div className="position-container">
                    <p className="top-text">{meme.topText}</p>
                    <p className="top-text">{meme.bottomText}</p>
                </div>
            </div>
        </main>
    )
}
export default Meme